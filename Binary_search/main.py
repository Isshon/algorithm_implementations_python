import math

import click


@click.command()
@click.option(
    "--list-to-search", "-l", type=list, default=[], nargs=1, help="List to search from"
)
@click.option("--key", "-k", nargs=1, help="Key to search")
def cli(key, list_to_search=None):
    result, position = Binary_search(key,list_to_search)
    if result:
        click.secho(f"Value found at position {position-3}", fg="green")
    else:
        click.secho("Value not found", err=True, fg="red")

def Binary_search(key, list_to_search=None):
    """Binary search algorithm"""
    if list_to_search is None:
        list_to_search = []
    list_to_search.sort()
    flag = False
    high = len(list_to_search) - 1
    low = 0
    position = 0
    while flag == False:
        mid_point = int(math.ceil((low + high) / 2))
        if key == list_to_search[mid_point]:
            flag = True
            position = mid_point
            break
        elif mid_point == high or mid_point == low:
            break
        elif key > list_to_search[mid_point]:
            low = mid_point + 1
        elif key < list_to_search[mid_point]:
            high = mid_point - 1
    return flag, position


if __name__ == "__main__":
    cli()
