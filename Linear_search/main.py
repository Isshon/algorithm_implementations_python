from typing import List

import click


@click.command()
@click.option(
    "--list-to-search", "-l", type=list, default=[], nargs=1, help="List to search from"
)
@click.option("--key", "-k", nargs=1, help="Key to search")
def linear_search(list_to_search, key):
    """Linear search algorithm"""
    flag: bool = False
    position: int = 0
    for idx, value in enumerate(list_to_search):
        if key == value:
            flag = True
            position = idx
    if flag:
        click.secho(f"Value found at position {position}", fg="green")
    else:
        click.secho("Value not found", err=True, fg="red")


if __name__ == "__main__":
    linear_search()
